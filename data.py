posts = []

class Post(object):
    def __init__(self, title='', content='', id=None):
        self.id = id
        self.title = title
        self.content = content

    def save(self):
        if self.id is None:
            posts.append(self)
            self.id = len(posts) - 1
        else:
            posts[self.id] = self

    @classmethod
    def get(cls, id):
        return posts[id] if len(posts) > id else None

    @classmethod
    def all(cls):
        return posts
