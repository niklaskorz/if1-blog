import pymysql.cursors

connection = pymysql.connect(host='localhost',
                             user='root',
                             password='',
                             db='blog',
                             charset='utf8mb4',
                             cursorclass=pymysql.cursors.DictCursor)

create_query = "INSERT INTO posts (title, content) VALUES (%s, %s)"
update_query = "UPDATE posts SET title = %s, content = %s WHERE id = %s"
get_query = "SELECT title, content FROM posts WHERE id = %s"
all_query = "SELECT id, title, content FROM posts"

class Post(object):
    def __init__(self, title='', content='', id=None):
        self.id = id
        self.title = title
        self.content = content

    def save(self):
        with connection.cursor() as cursor:
            if self.id is None:
                cursor.execute(create_query, (self.title, self.content))
                self.id = cursor.lastrowid
            else:
                cursor.execute(update_query, (self.title, self.content, self.id))
        connection.commit()

    @classmethod
    def get(cls, id):
        with connection.cursor() as cursor:
            cursor.execute(get_query, (id,))
            result = cursor.fetchone()

            if result is not None:
                return Post(result['title'], result['content'], id)

            return None

    @classmethod
    def all(cls):
        with connection.cursor() as cursor:
            cursor.execute(all_query)
            return [Post(row['title'], row['content'], row['id']) for row in cursor]
