from flask import Flask, render_template, request, redirect, url_for
from data import Post

app = Flask(__name__)

@app.route('/')
def index():
    posts = Post.all()
    return render_template('post.html', posts=posts)

@app.route('/post/<int:post_id>')
def post(post_id):
    post = Post.get(post_id)
    if post is None:
        return render_template('404.html', post_id=post_id)
    return render_template('post.html', posts=[post])

@app.route('/edit/new', methods=['GET', 'POST'])
def new_post():
    if request.method == 'POST':
        title = request.form['title']
        content = request.form['content']
        
        post = Post(title, content)
        post.save()

        return redirect(url_for('post', post_id=post.id))

    post = Post(id='new')
    return render_template('edit.html', post=post)

@app.route('/edit/<int:post_id>', methods=['GET', 'POST'])
def edit_post(post_id):
    if request.method == 'POST':
        title = request.form['title']
        content = request.form['content']
        
        post = Post(title, content, post_id)
        post.save()

        return redirect(url_for('post', post_id=post_id))

    post = Post.get(post_id)
    if post is None:
        return render_template('404.html', post_id=post_id)
    return render_template('edit.html', post=post)

if __name__ == '__main__':
    app.run(debug=True)
